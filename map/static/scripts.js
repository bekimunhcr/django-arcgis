require([
  "esri/map",
  "esri/layers/OpenStreetMapLayer",
  "dojo/domReady!"
], function (Map, OpenStreetMapLayer){

  var map, openStreetMapLayer;

  map = new Map("esri-map-container", {
    center: [108.2772, 14.0583],
    zoom: 6
  });
  openStreetMapLayer = new OpenStreetMapLayer();
  map.addLayer(openStreetMapLayer);
});